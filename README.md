# AI Competition

Hi and welcome to this AI competition.

> Note that the API is not finalized yet, small things may be changed

In this competition you will write an AI for a simple team shooting game.

***Want to submit?***

**To enter**: write an AI using the provided API and ....

## The game

There are two opposing teams.
A team consists of 5 tanks and a base.
These tanks can move around and shoot bullets.
When you're hit by a bullet of the other team you take damage.
When a tank has gathered enough points it can choose an upgrade (see API).

Each team has a base.
The goal of the game is to destroy the base of the other team.

## getting started

clone this repo from gitlab:

```bash
git clone https://gitlab.com/louisBoucquet/ai-competition
cd ai-competition
```

Open this folder in your editor and creat a file called `MyAI.js`

> **Note:** this file is in the `.gitignore` and won't be tracked

This way you can easily get bug fixes and api updates by pulling.

### How should your AI work?

The AI-function is just one function that gets information from the game and then sends commands back to the team.
This process happens every frame.

In your AI you will have access to a `team` object and a `world` object to get data about the game.

You will return an object that contain commands for each individual tank.

Your submission should look like this:

```js
function MyAI(params) {
    // Place your variables here
    // variables ...

    // return the function that is the actual AI
    return function(team, world) {
        // the command
        const command = []
        /* ... */
        return command
    }
}
```

### Creating the AI code

Now The AI

You can copy the template AI code above (paste it in `MyAI.js`) to get started.

First: let's move our tanks.

```js
function MyAI(params) {
    // remember, this is just a function that updates the team every frame
    return function(team, world) {
        // we need an array of commands for each of our tanks
        const commands = []

        // every tank just moves to the opposing tank now
        for (const enemy of world.enemies)
            commands.push({
                // move to a specific the enemy
                moveLocation: enemy.location
            })

        return commands
    }
}
```

Let's watch our code, open a devellop server an surf to the `/`

Only moving isn't that usefull so let's shoot some bullets.
To shoot bullets we need a location. Why not shoot to the tank we are moving to.

```js
function(team, world) {
    const commands = []

    for (const enemy of world.enemies)
        commands.push({
            moveLocation: enemy.location,
            // shoot your enemy
            shootLocation: enemy.location
        })

    return commands
}
```

This match probably takes a long time, but we can speed it up.
Normaly every frame there's only one "world tick" but van change this at the top of `MyAI.js`

```js
// now every frame, the world move 10 ticks
ticksPerFrame = 10

// AI stuff ...
```

Now the AI just updates every frame and has no larger goal.

```js
function makeAiA() {
    const enemiesPerPlayer = []
    for (let i = 0; i < 5; i++) {
        // choose a random enemy to destroy
        enemiesPerPlayer.push(Math.floor(Math.random() * 5))
    }
    return function(team, world) {
        const commands = []
        for (let i = 0; i < 5; i++)
            // shoot and move to your random enemy
            commands.push({
                moveLocation: world.enemies[enemiesPerPlayer[i]].location,
                shootLocation: world.enemies[enemiesPerPlayer[i]].location
            })
        return commands
    }
}
```

Even tho this was not usefull at all AI wise, this does show the basic concept of having some kind of memory.

## API

### Commands

To move, pass a `moveLocation` object, when you don't pass a `moveLocation` object, tanks stops moving.
A direction is just a location to which a tank wil go to in a straight line.

To shoot, pass a `shootLocation`, other wise you don't shoot.

> **Note: that by shooting you get recoil and you get blasted back**

If possible you can upgrade a trait by passing it in an array `upgrade`.
These are the possible upgrades

* **health-max**: Maximum health
* **health-regen**: The amount of health you regain per timestep
* **speed**: Max speed
* **bullet-speed**: Speed of bullets
* **bullet-damage**: The amount of damage your bullets do
* **bullet-reload**: The reload time
* **bullet-accuracy**: The accuracy

```js
return [
    {
        // moving to the top right
        moveLocation: { x, y },
        // to shoot, pass a location
        shootLocation: { x, y },
        // upgrades to apply
        upgrades: [
            "some-upgrade"
        ]
    },
    // ... 4 more tanks
]
```

### Information objects

> **Note that these objects only contain copied information**

If you would get access to the entire object you could just (for example) change the health to anything you want.

#### Team

This is what the `team` object might look like.

```js
const team = {
    tanks: [
        { /* see tanks */ }
    ], // 5 tanks
    base: { // the team base
        location: { x, y },
        health
    }
}
```

#### Player

This is what a `player` object might look like.

```js
const player = {
    // player's current location
    location: { x, y },
    // player's health
    health: 0,
    // player's max health
    healthMax: 100,
    // amount of upgrade points you can spend
    upgradePoints: 1,
    // upgrade progress
    upgrades: {
        "some-upgrade": 3,
        // other upgrades
    }
}
```

#### World

This is what the `world` object might look like.

```js
const world = {
    // the size of the world
    size: { x, y },
    // 5 enemy tanks
    enemies: [
        { /* see enemy */}
    ],
    // the enemy team's base
    enemyBase: {
        location: { x, y },
        health
    }
}
```

#### Enemy

This is what an `enemy` object might look like.

```js
const enemy = {
    // player's current location
    location: { x, y },
    // player's health
    health: 0
}
```
