class Coordinate {
    constructor(x, y) {
        this.x = x
        this.y = y
    }

    static zero() {
        return new Coordinate(0, 0)
    }

    static add(c1, c2) {
        return new Coordinate(c1.x + c2.x, c1.y + c2.y)
    }

    static sub(c1, c2) {
        return new Coordinate(c1.x - c2.x, c1.y - c2.y)
    }

    static multiplyNumber(c, n) {
        return new Coordinate(c.x * n, c.y * n)
    }

    static random(start, end = null) {
        if (end === null)
            return new Coordinate(Math.random() * start.x, Math.random() * start.y)

        const size = Coordinate.sub(end, start)
        return new Coordinate(start.x + Math.random() * size.x, start.y + Math.random() * size.y)
    }

    static randomFromZero(dist) {
        const lowerBound = new Coordinate(-dist, -dist)
        const upperBound = new Coordinate(dist, dist)

        return Coordinate.random(lowerBound, upperBound)
    }

    static dist(c1, c2) {
        return Math.sqrt(Math.pow(c2.x - c1.x, 2) + Math.pow(c2.y - c1.y, 2))
    }

    static length(c) {
        return Coordinate.dist(c, Coordinate.zero())
    }

    static normalize(c, normalLength = 1) {
        const currLength = Coordinate.length(c)
        if (currLength === 0)
            return c
        else if (normalLength === 0)
            return Coordinate.zero()
        return new Coordinate(c.x / currLength * normalLength, c.y / currLength * normalLength)
    }

    static perpendicular(vector) {
        if (vector.x !== 0) {
            return Coordinate.normalize(new Coordinate(-vector.y / vector.x, 1), 1)
        } else if (vector.y !== 0) {
            return Coordinate.normalize(new Coordinate(1, -vector.x / vector.y), 1)
        }
    }

    static keepInBounds(c, bounds) {
        c = Object.assign({}, c)
        if (c.x < 0) c.x = 0
        if (c.y < 0) c.y = 0

        if (c.x > bounds.x) c.x = bounds.x
        if (c.y > bounds.y) c.y = bounds.y

        return c
    }

    static getDirection(direction) {
        switch (direction) {
            case "left":
                return { x: -1, y: 0 }
            case "right":
                return { x: 1, y: 0 }
            case "up":
                return { x: 0, y: -1 }
            case "down":
                return { x: 0, y: 1 }
            default:
                return { x: 0, y: 0 }
        }
    }
}
