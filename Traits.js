class Traits {
    constructor() {
        this.initial = {
            "health-max": 100,
            "health-regen": 0.1,
            "speed": 5,
            "bullet-speed": 7,
            "bullet-damage": 8,
            "bullet-reload": 25,
            "bullet-accuracy": 1
        }

        this.upgradePoints = 0

        this.actual = {
            "health-max": 0,
            "health-regen": 0,
            "speed": 0,
            "bullet-speed": 0,
            "bullet-damage": 0,
            "bullet-reload": 0,
            "bullet-accuracy": 0
        }

        this.max = {
            "health-max": 2,
            "health-regen": 2,
            "speed": 2,
            "bullet-speed": 2,
            "bullet-damage": 2,
            "bullet-reload": 2,
            "bullet-accuracy": 2
        }
    }

    upgrade(upgradeName) {
        const value = this.actual[upgradeName]

        if (value !== undefined && value < 8 && this.upgradePoints > 0) {
            this.actual[upgradeName]++
            this.upgradePoints--
            console.log("upgrade", upgradeName, this.actual[upgradeName])

            if (Object.keys(this.actual).every(key => this.actual[key] == 5)) {
                console.log("FULL UPGRADE!!!")
            }
        }
    }

    getData(upgrade) {
        return this.initial[upgrade] * (1 + this.actual[upgrade] * (this.max[upgrade] - 1) / 5)
    }

    getInformation() {
        return Object.assign({}, this.actual)
    }

    getInformationActual() {
        const out = {}

        for (const key of Object.keys(this.actual))
            out[key] = this.getData(key)

        return out
    }
}
