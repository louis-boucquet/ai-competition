class World {
    constructor(size, { A, B }) {
        this.size = size

        this.teamA = new Team(this, true, A)
        this.teamB = new Team(this, false, B)

        this.counter = 0
    }

    tick() {
        this.teamA.update(this)
        this.teamB.update(this)

        this.counter++

        const totalScoreA = this.teamA.tanks
            .map(tank => tank.score)
            .reduce((totalScore, score) => totalScore + score, 0)

        const totalScoreB = this.teamB.tanks
            .map(tank => tank.score)
            .reduce((totalScore, score) => totalScore + score, 0)

        console.log("A", totalScoreA, "B", totalScoreB)
    }

    render(ctx) {
        const totalScoreA = this.teamA.tanks
            .map(tank => tank.score)
            .reduce((totalScore, score) => totalScore + score, 0)

        const totalScoreB = this.teamB.tanks
            .map(tank => tank.score)
            .reduce((totalScore, score) => totalScore + score, 0)

        const maxScore = Math.max(totalScoreA, totalScoreB)

        this.teamA.render(ctx)
        this.teamB.render(ctx)

        ctx.fillStyle = "DARKGRAY"

        ctx.beginPath()
        ctx.fillRect(0, this.size.y, this.size.x / 2 * 0.9, 25)
        ctx.fill()

        ctx.fillStyle = "FORESTGREEN"

        ctx.beginPath()
        ctx.fillRect(0, this.size.y, this.size.x / 2 * totalScoreA / maxScore * 0.9, 25)
        ctx.fill()

        ctx.fillStyle = "DARKGRAY"

        ctx.beginPath()
        ctx.fillRect(this.size.x / 2, this.size.y, this.size.x / 2 * 0.9, 25)
        ctx.fill()

        ctx.fillStyle = "FORESTGREEN"

        ctx.beginPath()
        ctx.fillRect(this.size.x / 2, this.size.y, this.size.x / 2 * totalScoreB / maxScore * 0.9, 25)
        ctx.fill()
    }

    running() {
        return this.teamA.base.health > 0 && this.teamB.base.health > 0
    }

    getOtherTeam(team) {
        return team === this.teamA ? this.teamB : this.teamA
    }

    getInformation(team) {
        const enemyTeam = this.getOtherTeam(team)

        return {
            // the size of the wordl
            size: Object.assign({}, this.size),
            // 5 enemy tanks
            enemies: enemyTeam.tanks.map(p => p.getInformationEnemies()),
            // the enemy team's base
            enemyBase: enemyTeam.base.getInformation()
        }
    }
}
