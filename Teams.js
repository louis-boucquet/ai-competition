class Team {
    constructor(world, onLeft, updater) {
        this.updater = updater

        this.tanks = []

        // left-right related shit
        const xTrans = onLeft ? x => x : x => world.size.x - x
        this.colors = onLeft ? {
            fillStyle: "DODGERBLUE",
            strokeStyle: "STEELBLUE",
            bulletColor: "DODGERBLUE"
        } : {
                fillStyle: "FIREBRICK",
                strokeStyle: "DARKRED",
                bulletColor: "FIREBRICK"
            }

        this.base = new TeamBase({ x: xTrans(60), y: world.size.y / 2 })

        for (let i = 0; i < 5; i++)
            this.tanks.push(new Tank(
                {
                    x: xTrans(45 + 100),
                    y: world.size.y / 6 * (i + 1)
                }, 0, xTrans(2000 - 145)))
    }

    spawnTank() {
        return new Tank(Coordinate.random(this.size))
    }

    reSpawn(deadTank) {
        const newPlayer = new Tank(this.base.location, deadTank.score / 2)

        this.tanks = this.tanks.map(p => p === deadTank ? newPlayer : p)
    }

    update(world) {
        const commands = this.updater(this.getInformation(), world.getInformation(this))

        this.base.update(world, this)

        if (commands)
            for (let i = 0; i < 5; i++)
                this.tanks[i]
                    .update(Object.assign(Tank.getDefaults(), commands[i]), world, this)
        else
            for (let i = 0; i < 5; i++)
                this.tanks[i]
                    .update(Tank.getDefaults(), world, this)
    }

    render(ctx) {
        this.base.render(ctx, this.colors)

        for (const tank of this.tanks)
            tank.render(ctx, this.colors)
    }

    getInformation() {
        return {
            tanks: this.tanks.map(p => p.getInformation()),
            base: this.base.getInformation()
        }
    }
}
