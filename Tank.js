class Tank extends Actor {
    constructor(location, score = 0) {
        console.log("new Tank with score", score)
        super(location)
        this.lastLocation = this.location

        // non move shit
        this.traits = new Traits()

        this.traits.upgradePoints = Math.round(score / 100)

        this.score = score
        this.pointsUntilNextUpgrade = 100

        this.health = this.traits.getData("health-max")

        // bullets
        this.bullets = new Set()
        this.lastShot = 0
    }

    update({ moveLocation, shootLocation, upgrades }, world, team) {
        // MOVEMENT
        this.lastLocation = this.location

        const moveDirection = moveLocation ? Coordinate.sub(moveLocation, this.location) : Coordinate.zero()

        let speed = this.traits.getData("speed")

        if (shootLocation)
            speed *= 0.8

        speed = Math.min(speed, Coordinate.length(moveDirection))

        const speedVector = Coordinate.normalize(moveDirection, speed)

        this.location = Coordinate.add(this.location, speedVector)

        this.location = Coordinate.keepInBounds(this.location, world.size)

        // ENEMY INTERACTION
        const enemies = world.getOtherTeam(team)

        // keep enemies out of the base
        if (Coordinate.dist(enemies.base.location, this.location) < 100) {
            const direction = Coordinate.sub(this.location, enemies.base.location)
            this.location = Coordinate.add(enemies.base.location, Coordinate.normalize(direction, 100))
        }

        // TAKING DAMGAGE
        for (const enemy of enemies.tanks) {
            for (const bullet of enemy.bullets) {
                if (Coordinate.dist(bullet.location, this.location) < 15) {
                    this.health -= enemy.traits.getData("bullet-damage")
                    bullet.remove()

                    if (this.health <= 0) {
                        enemy.score += this.score / 2
                        team.reSpawn(this)
                    }
                }
            }
        }

        // SHOOTING
        this.lastShot++

        if (shootLocation && this.lastShot > this.traits.getData("bullet-reload")) {
            this.lastShot = 0
            const shootDirection = Coordinate.sub(shootLocation, this.location)
            const shootDirectionNormalized = Coordinate.normalize(shootDirection, this.traits.getData("bullet-speed"))
            const shootDirectionNormalizedRandom = Coordinate.add(shootDirectionNormalized, Coordinate.randomFromZero(1 / this.traits.getData("bullet-accuracy")))

            const bullet = new Bullet(this.location, shootDirectionNormalizedRandom)
            bullet.remove = () => {
                this.bullets.delete(bullet)
                this.onDealDamage(this.traits.getData("bullet-damage"))
            }

            this.bullets.add(bullet)
        }

        for (const bullet of this.bullets)
            bullet.update()

        // HEALTH SHIT

        if (this.health < this.traits.getData("health-max")) {
            this.health = Math.min(this.traits.getData("health-max"), this.health + this.traits.getData("health-regen"))
        }

        // UPGRADING

        for (const upgrade of upgrades) {
            this.traits.upgrade(upgrade)
        }
    }

    render(ctx, colors) {
        super.render(ctx, colors, 15)

        for (const bullet of this.bullets)
            bullet.render(ctx, colors)
    }

    onDealDamage(damage) {
        this.score += damage
        this.pointsUntilNextUpgrade -= damage

        while (this.pointsUntilNextUpgrade < 0) {
            this.pointsUntilNextUpgrade += 100
            this.traits.upgradePoints++
        }
    }

    getMaxHealth() {
        return this.traits.getData("health-max")
    }

    getInformation() {
        return {
            location: Object.assign({}, this.location),
            speed: Coordinate.sub(this.location, this.lastLocation),
            health: this.health,
            score: this.score,
            upgradePoints: this.traits.upgradePoints,
            upgrades: this.traits.getInformation(),
            traits: this.traits.getInformationActual()
        }
    }

    getInformationEnemies() {
        return {
            location: Object.assign({}, this.location),
            speed: Coordinate.sub(this.location, this.lastLocation),
            health: this.health,
            score: this.score,
            bullets: Array.from(this.bullets).map(b => b.getInformation()),
            traits: this.traits.getInformationActual()
        }
    }

    static getDefaults() {
        return {
            upgrades: []
        }
    }
}
