function defensiveAI() {
    // HELPER

    function getWithinRange(array, location, range, coordinateMapper = el => el.location) {
        return array
            .filter(e => Coordinate.dist(coordinateMapper(e), location) < range)
    }

    function getClosest(array, location, coordinateMapper = el => el.location) {
        return array.reduce((l, c) => Coordinate.dist(coordinateMapper(l), location) < Coordinate.dist(coordinateMapper(c), location) ? l : c)
    }

    function getAverage(array, coordinateMapper = el => el) {
        return array
            .map(coordinateMapper)
            .map(t => Coordinate.multiplyNumber(t, 1 / array.length))
            .reduce(Coordinate.add, Coordinate.zero())
    }

    function hasToRetreat(team, world, tank) {
        return tank.health / tank.traits["health-max"] < 0.2
    }

    function isSafe(team, world, tank) {
        return tank.health / tank.traits["health-max"] > 0.7
    }

    function attackFormADistance(enemyLocation, tankLocation) {
        const dist = Coordinate.dist(tankLocation, enemyLocation)
        if (dist > 300) {
            return {
                moveLocation: enemyLocation,
                shootLocation: enemyLocation,
                upgrades
            }
        } else if (dist < 50) {
            const direction = Coordinate.sub(tankLocation, enemyLocation)
            return {
                shootLocation: enemyLocation,
                moveLocation: Coordinate.add(tankLocation, direction),
                upgrades
            }
        } else {
            return {
                shootLocation: enemyLocation,
                upgrades
            }
        }
    }

    // STATES
    function retreat(team, world, tank, i) {
        if (isSafe(team, world, tank))
            states[i] = attack

        // if you're safe
        if (Coordinate.dist(team.base.location, tank.location) < 15) {
            // enemies in range
            const enemies = getWithinRange(world.enemies, tank.location, range)

            // if there's anyone close enough to shoot at
            if (enemies.length > 0) {
                return {
                    shootLocation: enemies[0].location,
                    upgrades
                }
            }
        } else {
            return {
                moveLocation: team.base.location,
                upgrades
            }
        }
    }

    function attack(team, world, tank, i) {
        if (hasToRetreat(team, world, tank))
            states[i] = retreat

        // get all attacking tanks
        const attacking = [...team.tanks.keys()]
            .filter(i => states[i] === attack)
            .map(i => team.tanks[i])

        const avgPos = getAverage(attacking, el => el.location)

        // enemies in range
        const enemies = getWithinRange(world.enemies, avgPos, range)

        if (enemies.length > 0) {
            return attackFormADistance(enemies[0].location, tank.location)
        } else {
            // attack closest enemy to base
            const closest = getClosest(world.enemies, team.base)

            return {
                moveLocation: closest.location,
                upgrades
            }
        }
    }

    const upgrades = [
        "bullet-damage",
        "bullet-reload",
        "bullet-speed",
        "bullet-accuracy",
        "health-max",
        "speed",
        "health-regen",
        "acceleration"
    ]

    const states = []

    for (let i = 0; i < 5; i++)
        states.push(retreat)

    const range = 500;

    return function (team, world) {

        const commands = []

        for (let i = 0; i < 5; i++) {

            const me = team.tanks[i]

            const command = states[i](team, world, me, i)

            commands.push(command)

        }
        return commands
    }
}

function offensiveAI() {
    const upgrades = [
        "bullet-damage",
        "health-max",
        "bullet-reload",
        "bullet-speed",
        "bullet-accuracy",
        "health-regen",
        "speed",
    ]
    return (team, world) => {
        const commands = []
        for (let i = 0; i < 5; i++)
            commands.push({
                moveLocation: world.enemies[i].location,
                shootLocation: world.enemies[i].location,
                upgrades
            })
        return commands
    }
}

function baseAI() {
    const upgrades = [
        "bullet-damage",
        "health-max",
        "bullet-reload",
        "health-regen",
        "bullet-speed",
        "bullet-accuracy",
        "speed",
    ]
    return (team, world) => {
        const commands = []
        for (let i = 0; i < 5; i++)
            commands.push({
                shootLocation: world.enemyBase.location,
                moveLocation: world.enemyBase.location,
                upgrades
            })
        return commands
    }
}
