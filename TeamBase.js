class TeamBase extends Actor {
    constructor(location) {
        super(location)
        this.health = 3000
    }

    update(world, team) {
        // ENEMY INTERACTION
        const enemies = world.getOtherTeam(team)

        for (const enemy of enemies.tanks) {
            for (const bullet of enemy.bullets) {
                if (Coordinate.dist(bullet.location, this.location) < 30) {
                    this.health -= enemy.traits.getData("bullet-damage")
                    bullet.remove()
                }
            }
        }

        if (this.health <= 0)
            this.health = 0
    }

    render(ctx, colors) {
        super.render(ctx, colors, 30)
    }

    getMaxHealth() {
        return 3000
    }

    getInformation() {
        return {
            location: Object.assign({}, this.location),
            health: this.health
        }
    }
}
