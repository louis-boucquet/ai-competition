"use strict";

document.addEventListener("DOMContentLoaded", init)

const worldDimensions = { x: 2000, y: 1000 }
const world = new World(worldDimensions, { A: MyAI(), B: makeAI() })

function init() {
    const ctxDimensions = Object.assign({}, worldDimensions)
    ctxDimensions.y += 50
    const ctx = getNewCtx(ctxDimensions)

    // world.tick()
    // world.render(ctx)

    function update() {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)

        for (let i = 0; i < ticksPerFrame; i++)
            world.tick()

        world.render(ctx)

        if (world.running())
            window.requestAnimationFrame(update)
    }

    update()
}

function getNewCtx({ x, y }, container = document.querySelector("body")) {
    const canvas = document.createElement("canvas")
    canvas.width = x
    canvas.height = y
    container.innerHTML = ""
    container.appendChild(canvas)

    return canvas.getContext("2d")
}
