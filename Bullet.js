class Bullet {
    constructor(location, speed) {
        this.location = location
        this.speed = speed

        this.lifeTime = 0
    }

    update() {
        this.location = Coordinate.add(this.location, this.speed)

        this.lifeTime++

        if (this.lifeTime > 70)
            this.remove()
    }

    render(ctx, { bulletColor }) {
        ctx.fillStyle = bulletColor

        ctx.beginPath();
        ctx.arc(this.location.x, this.location.y, 5, 0, 2 * Math.PI)
        ctx.fill();
    }

    getInformation() {
        return {
            location: Object.assign({}, this.location),
            speed: Object.assign({}, this.speed)
        }
    }
}