class Actor {
    constructor(location) {
        this.location = location
    }

    render(ctx, { fillStyle, strokeStyle }, size) {
        ctx.fillStyle = fillStyle
        ctx.strokeStyle = strokeStyle
        ctx.lineWidth = 5;

        ctx.beginPath();
        ctx.arc(this.location.x, this.location.y, size, 0, 2 * Math.PI)
        ctx.stroke();
        ctx.fill();

        ctx.fillStyle = "DARKGRAY"

        ctx.beginPath();
        ctx.fillRect(this.location.x - size * 1.5, this.location.y - size - 16, size * 3, 8)
        ctx.fill();

        ctx.fillStyle = "FORESTGREEN"

        ctx.beginPath();
        ctx.fillRect(this.location.x - size * 1.5, this.location.y - size - 16, size * 3 * this.health / this.getMaxHealth(), 8)
        ctx.fill();
    }

    getMaxHealth() {
        return 1
    }
}